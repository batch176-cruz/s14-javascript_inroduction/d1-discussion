
console.log('Hello World from external JS file!');

// Writing comments
	// single-line ( ctrl + /)
	/* multi-line (ctrl + shift + /) */


/*
	Syntax and Statements 

	Syntax - set of rules how codes should be written correctly


	Statement - set of intructions, ends with semicolon
*/

// alert('Good Morning!')

/*
	Variables
		variable - a container that holds a value/data
		value - a data that you can assign to a variable
*/

/*
 myName = "Joy";
 console.log(myName);
*/

 	let car = "rolls royce";
 	let firstName = "Carlo";
 	let lastName = "Cruz";
 	let Batch = "Batch 176";

 	let fullName = "Hi, my name is" + " " + firstName + " " + lastName + "." + " " + "I am from" + " " + Batch

 	// ES6 update
 		// template literals - backticks and ${}

 	let fullName2 = `Hi, my name is ${firstName + " " + lastName}.I am ${10 + 16} years old. I am from ${Batch}`

 	console.log(fullName)
 	console.log(fullName2)


// Q: Can we re-assign a new value to a variable? - Y
	let year2022 = "Tom";
	console.log(year2022)

	year2022 = "Aaron";
	console.log(year2022)

// Q: Can we re-declare a variable? - No, we can only reassign a new value but not re-declare same variable.

	// let job = 'instructor';

	// job = 'software engineer';

// Why is variable important?

let brand = `ROG`;
brand = `Predator`;

console.log(brand);	//line100
console.log(brand);	//line200
console.log(brand);	//line300
console.log(brand);	//line400
console.log(brand);	//line500
console.log(brand);	//line600
console.log(brand);	//line700
console.log(brand);	//line800
console.log(brand);	//line900
console.log(brand);	//line1000

// Why is declaring a variable still important if variable would still work even without using let keyword?
myname = `christiana`;
console.log(myname);

	// variable can still be a container even without a present value
		let container;
		console.log(container);	// undefined

		// what if you want a new variable but without declaring/initializing a value? - It will not work, var needs to be declared first.

		// address;				not defined
		// console.log(address);


/* Constant
		- a type of variable that holds data/value
		- using const keyword
		- we cannot re-assign a new valur to a constant variable
*/

	const PI = 3.14;
	console.log(PI);

	const SECONDS = 60;
	const MINUTES = 60;
	const HOURS = 24;

	const sentence = `One minute has ${SECONDS} seconds, one hour has ${MINUTES} minutes, and one day has ${HOURS} hours.`

	console.log(sentence);

	// SECONDS = 60 * 2;	error because re-assigning a new value to a constant variable is not accepted by JS


	// Will declaring a constant variable without a value works? - No, it should always come with a value because the concept must be constant re-assignment should not be applied to constant variables.
	// const container2;	error
	// container 2 = "adobo";	cannot reassign to constant variable


/*	Data Types
		- a way to sort out data/values according to their type

	1. String
		- sequence of characters
		- always wrapped arount with backticks or quitation marks
		- if no quotation or backticks, JS will comfuse the value with variable

	Example:

		let flower = `Rose`;
		let mobile = `+639123456789`;


	2. Number\
		- whole numbers (integer) & decimals (float)

	Example:

		let num = 639123456789;


	3. Boolean
		- logical tyjpe that can only be true or false;
		- use for taking decisions
		- with the help of operators, we can come up a value of true or false based on the evaluation

	Example:

		let isEarly = true;
	console.log(isEarly);

	let areLate = false;
	console.log(areLate);


	4. Undefined
		- value not yet assigned, or it could be empty value.

	Example:

		let data;
		console.log(data);			//undefined (accepted by JS)

		newData;
		console.log(newData);		//not defined (not accepted by JS)


	5. Null
		- also means empty
		- it is an assigned value to a variable that is used to represent an empty value

	Example:

	let spouse = null;


	// primitive data types

	// reference data types


	6. Object
		- it has curly braces, properties & values
		- a single variable can hold multiple different types of data

*/


	let person = {
		// property: value
		name: "Tim",
		age: 29,
		address: {
			city: "Cainta",
			province: "Rizal"
		},
		isSoftwareEngineer: true,
		email: ["tim@mail.com", "therera@mail.com"],
		spouse: null
	}
	
	// Special type of object
	/* Array
		- it has square brackets, and using index as a reference to each element inside the bracket
		- a single variable that contain a collection of related data/values
	*/

	let grades = [91, 93, 95, 98];

	// This example is correct by syntax but not recommended since elements in an array should be relative
	//  let random = ["bag", 100, true, undefined, null];


	/*	typeof() 
			- operator helps us determine what type of data are we working with
			- returns a string value
	*/

	console.log( "mercedes" );			// mercedes
	console.log( typeof("mercedes") );	// string

	console.log ( 3.14 );				// 3.14
	console.log( typeof(3.14) );		// number

	console.log( true );				// true
	console.log( typeof(true) );		// boolean


/*	Functions
		- reusable pieces of code
	
	create hello world and display in the console 10x in hour

	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");


	create hello world and display in the console 100x in hour
	
	create hello world and display in the console 1000x in hour
*/

	function sayHello(){ //codeblock
		// statement
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
	}

	// sayHello();
	// sayHello();
	// sayHello();

/*	Function has two parts

	Syntax:

		function <fname>(){
	

		}

		<fname()

	1. Function Declaration
		- function keyword
		- function name
		- () = parameters
		- code block

	2. Function Invocation
		- function name
		- () = arguments
*/	

//	Simple function

	function intro(name, job){

		// console.log(`My name is Joy. I am a Full Stack Web Developer.`)

		console.log(`My name is ${name}. I am a ${job}.`)
	}

	intro("Joy", "Full Stack Developer");
	intro("Dexter", "Frontend Developer");
	intro(firstName, "Software Engineer");
	intro("Earl", "Full Stack Developer");


/*	Mini Activity

	Create a function that will accept 3 numbers and display the total sum of these 3 numbers in the console.

	Send the screenshot of your codes in groupchat

*/


function add(firstNum, secondNum, thirdNum){
	console.log(firstNum + secondNum + thirdNum)
}

add(1, 2, 3);
add(100, 150, 200);
add(50, 1 , 1);

//	Return keyword

	function product(a, b){
		console.log(a * b);

		return a * b;
	}

	// let result = product(2, 4);
	// console.log(result)

	// console.log(product(2, 4))




